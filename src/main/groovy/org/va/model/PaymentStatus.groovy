package org.va.model

/**
 * This enum defines the different statusses a Payment can have.
 */
public enum PaymentStatus {

    NEW("Nieuw"),
    IGNORED("Genegeerd"),
    MATCHED("Verwerkt")

    String description

    PaymentStatus(String description) {
        this.description = description
    }

    String toString() { return description }
    String getKey() { return name() }
}