package org.va.model

/**
 * The Tariff defines the role of the person when paying for a
 */
enum Tariff {

    FULL("Vol"),
    CREW("Monster/Crew"),
    MINOR("Kinderen 12-15"),
    FREE("Gratis"),
    ADJUSTED("Aangepast")

    String description

    Tariff(String description) {
        this.description = description
    }

    String toString() {
        return description
    }

    String getKey() {
        return name()
    }
}
