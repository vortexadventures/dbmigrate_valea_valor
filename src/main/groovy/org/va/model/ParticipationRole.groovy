package org.va.model

/**
 * A ParticipationRole defines what the person does on the event when it has started.
 */
public enum ParticipationRole {

    PLAYER("Speler"),
    MONSTER("Monster"),
    REFEREE("Spelleider"),
    STAFF("Staf"),
    MEDIC("EHBO"),
    OTHER("Anders")

    String description

    ParticipationRole(String description) {
        this.description = description
    }

    String toString() {
        return description
    }

    String getKey() {
        return name()
    }
}