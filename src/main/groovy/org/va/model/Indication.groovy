package org.va.model

/**
 * This class defines a container for an indication that tells something about something else, for instance a purchase.
 */
class Indication {

    String text
    IndicationLevel level

    Indication(String text, IndicationLevel level) {
        this.text = text
        this.level = level
    }
}
