package org.va.valor

import org.joda.time.LocalDateTime

/**
 * This domain class defines a user that can log into the system.
 */
class User {

    long id
    Person person
    String hashedPassword
    String salt
    int failedLoginAttempts
    LocalDateTime lockedOutUntil

    static constraints = {
    }


}
