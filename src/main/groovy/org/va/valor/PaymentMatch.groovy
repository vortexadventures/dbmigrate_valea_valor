package org.va.valor

/**
 * This domain object is a match between a Payment and a Purchase, meaning that part of or the whole of the money from a Payment is used to pay for a Purchase.
 */
class PaymentMatch {

    Long id
    Payment payment
    Purchase purchase
    BigDecimal amount

    static constraints = {
    }
    static mapping = {
        payment lazy: false
        purchase lazy: false
    }
}
