package org.va.valor

import org.joda.time.LocalDate

/**
 * This class defines the record of club membership by a person. A single person can have multiple memberships over a period of time if
 * the person stopped its membership and later started one again.
 */
class Membership {

    long id
    Person person
    LocalDate start
    LocalDate end
    boolean formSigned

    static constraints = {
        id(nullable: false)
        person(nullable: false)
        start(nullable: false)
        end(nullable: true)
        formSigned(nullable: false)
    }
}
