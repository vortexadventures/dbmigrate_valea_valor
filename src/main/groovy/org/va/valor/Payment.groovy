package org.va.valor

import org.joda.time.LocalDate
import org.va.model.PaymentStatus
import org.va.model.PaymentType

/**
 * This domain object represents a payment that took place in the past (as opposed to invoices or future refunds etc).
 */
class Payment {

    long id

    // For migration purposes
    long valeaid

    PaymentStatus status
    PaymentType type
    BigDecimal amount
    BigDecimal amountOpen
    LocalDate bookingDate

    String bankBookingCode
    String description1
    String description2
    String description3
    String description4
    String description5
    String description6

    /** This indicates which (bank account / cash register / ideal account / other source) this payment belongs to. */
    String ourSource

    /** Bank payment fields */
    String theirAccountIban
    String theirAccountName

    /** This optional field defines who is the other party - only in case we know this. */
    Person theirPerson

    String idealReference

    /** This is for cash transactions and indicates who accepted/handled the cash for VA. */
    Person handler


    static constraints = {
        bankBookingCode(nullable: true)
        description1(length: 1024)
        description2(nullable: true)
        description3(nullable: true)
        description4(nullable: true)
        description5(nullable: true)
        description6(nullable: true)
        theirAccountIban(nullable: true)
        theirAccountName(nullable: true)
        theirPerson(nullable: true)
        handler(nullable: true)
        idealReference(nullable: true)
    }

    /** Input from ideal is sometimes longer than 500 characters. */
    static mapping = {
        description1(length: 2048)
        description2(length: 2048)
        description3(length: 2048)
        description4(length: 2048)
        description5(length: 2048)
        description6(length: 2048)
    }

    String theirShortName() {
        if (theirAccountName == null) {
            return ""
        }
        if (theirAccountName.length() > 32) {
            return theirAccountName.substring(0, 32)
        }
        return theirAccountName
    }

    String fullDescription() {
        boolean seperateLines = seperateLines()

        StringBuilder builder = new StringBuilder()
        builder.append(description1)
        addToFullDescription(builder, description2, seperateLines)
        addToFullDescription(builder, description3, seperateLines)
        addToFullDescription(builder, description4, seperateLines)
        addToFullDescription(builder, description5, seperateLines)
        addToFullDescription(builder, description6, seperateLines)
        return builder.toString()
    }

    boolean seperateLines() {
        if (type == PaymentType.IDEAL) {
            return false
        }
        return !("cb".equalsIgnoreCase(bankBookingCode) || "bg".equalsIgnoreCase(bankBookingCode));
    }

    void addToFullDescription(StringBuilder builder, String input, boolean seperateLines) {
        if (input == null) {
            return
        }
        if (seperateLines) {
            builder.append(" ")
        }
        builder.append(input)
    }
}
