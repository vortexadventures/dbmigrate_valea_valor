package nl.va.valeamigrate.repo

import nl.va.valeamigrate.dbutil.DateMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementCreator
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.jdbc.support.KeyHolder
import org.springframework.stereotype.Repository
import org.va.model.PaymentStatus
import org.va.valor.*

import javax.sql.DataSource
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * This class defines a data repository for the Valor classes
 */
@Repository
class ValorRepo implements InitializingBean {
    def static log = LoggerFactory.getLogger(ValorRepo)

    @Autowired DataSource valorDataSource;

    JdbcTemplate template

    static RowMapper paymentRowMapper = new RowMapper<Payment>() {
        @Override
        public Payment mapRow(ResultSet rs, int row) throws SQLException {
            Payment info = new Payment()

            info.id = rs.getLong("id")
            info.amount = rs.getBigDecimal("amount")
            info.amountOpen = rs.getBigDecimal("amount_open")
            String statusAsString = rs.getString("status")
            info.status = PaymentStatus.valueOf(statusAsString)
            info.bookingDate = DateMapper.toLocalDate(rs, "booking_date")
            long theirPersonId = rs.getLong("their_person_id")
            info.theirPerson = new Person(id: theirPersonId)

            return info
        }
    }

    static RowMapper purchaseRowMapper = new RowMapper<Purchase>() {
        @Override
        public Purchase mapRow(ResultSet rs, int row) throws SQLException {
            Purchase info = new Purchase()

            info.id = rs.getLong("id")

            long productId = rs.getLong("product_id")
            info.product = new Product(id: productId)
            info.amount = rs.getBigDecimal("amount")
            info.amountOpen = rs.getBigDecimal("amount_open")
            long personId = rs.getLong("person_id")
            info.person = new Person(id: personId)
            info.purchaseDate = DateMapper.toLocalDate(rs, "purchase_date")

            return info
        }
    }
    void afterPropertiesSet() throws Exception {
        this.template = new JdbcTemplate(valorDataSource)
    }

    def save(Object object) {
        def columns = FieldMapper.columList(object.class)
        def values = FieldMapper.valueList(object)
        def tableName = FieldMapper.camelCaseToUnderscore(object.class.simpleName).substring(1)
        def sql = "insert into ${tableName} (${columns}) values (${values});"
        log.debug(sql)

        String[] columnNamesToReturn = ["id"].toArray()
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(sql.toString(), columnNamesToReturn);
                        return ps;
                    }
                },
                keyHolder);

        long pk = keyHolder.getKey()
        object.id = pk;

        return object
    }

    boolean deelnemerWithIdIsMigrated(long valeaid) {
        String sql = "select count(*) from person where valeaid=?"
        def count = template.queryForObject(sql, Long, valeaid)
        return count == 1
    }

    boolean deelnemerWithPlinIsMigrated(String plin) {
        String sql = "select count(*) from person where plin=?"
        def count = template.queryForObject(sql, Long, plin)
        return count == 1
    }

    long getPersonIdByValeaId(long valeaid) {
        String sql = "select id from person where valeaid=?"
        return template.queryForObject(sql, Long, valeaid)
    }

    def runSql(String sql) {
        log.debug(sql)
        template.execute(sql)
    }

    def runSqlUpdate(String sql, Object... args) {
        log.debug(sql)
        template.update(sql, args)
    }

    long getPersonIdByPlin(String plin) {
        String sql = "select id from person where plin=?";
        return template.queryForObject(sql, Long, plin)
    }

    Person getIdOnlyPersonForPlin(String plin) {
        long id = getPersonIdByPlin(plin)
        return new Person(id: id)
    }

    def delete(Object entity) {
        String sql = "delete from ${entity.class.simpleName} where id=?"
        template.update(sql, entity.id)
    }

    Event getIdOnlyEventByName(String valorEventName) {
        String sql = "SELECT id from event where name=?"
        Long id = template.queryForObject(sql, Long, valorEventName)
        return new Event(id: id)
    }

    Product getMinimalProductByName(String productName) {
        String sql = "SELECT id FROM product WHERE name=?"
        Long id = template.queryForObject(sql, Long, productName)

        sql = "SELECT price FROM product WHERE name=?"
        BigDecimal price = template.queryForObject(sql, Long, productName)
        return new Product(id: id, price: price)
    }

    Payment fetchPaymentByValeaId(long valeaBoekingId) {
        String sql = "select * from payment where valeaid = ?"
        Object[] args = [valeaBoekingId]
        return template.queryForObject(sql, args, paymentRowMapper)
    }

    Purchase fetchPurchaseByValeaId(long valeaId) {
        def sql = "SELECT * FROM purchase where valeaid = ?"
        Object[] args = [valeaId]
        return template.queryForObject(sql, args, purchaseRowMapper)
    }

    void updatePaymentAmountOpen(Payment payment) {
        def sql = "UPDATE payment SET amount_open = ? where id = ?"
        Object[] args = [payment.amountOpen, payment.id]
        template.update(sql, args)
    }

    void updatePurchaseAmountOpen(Purchase purchase) {
        def sql = "UPDATE purchase SET amount_open = ? where id = ?"
        Object[] args = [purchase.amountOpen, purchase.id]
        template.update(sql, args)
    }

    boolean existsPaymentWithValeaId(long valeaId) {
        String sql = "select count(*) from payment where valeaid = ?"
        Object[] args = [valeaId]
        long count = template.queryForLong(sql, args)
        return count == 1;
    }
}
