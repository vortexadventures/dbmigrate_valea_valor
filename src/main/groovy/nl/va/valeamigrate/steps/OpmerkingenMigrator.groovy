package nl.va.valeamigrate.steps

import nl.va.valeamigrate.repo.ValorRepo
import nl.va.valeamigrate.valea.Deelnemer
import nl.va.valeamigrate.valea.Opmerking
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.va.valor.Memo
import org.va.valor.Person

/**
 * This class migrates the deelnemers.
 */
@Component
class OpmerkingenMigrator {

    def static log = LoggerFactory.getLogger(OpmerkingenMigrator)

    @Autowired ValorRepo valorRepo

    int memosMigratedOldStyle = 0
    int memosMigratedNewStyle = 0

    Person erik
    Person gerrit

    void run() {
        long startTime = System.currentTimeMillis()

        List<Integer> ids = Opmerking.listIds()
        log.info("Migrating ${ids.size()} new style memos.")
        ids.each({migrateNewStyleMemo(it)})

        List<Integer> deelnemerIds = Deelnemer.listIds();
        log.info("Checking ${deelnemerIds.size()} for old style memos.")
        deelnemerIds.each({migrateOldStyleMemo(it)})

        long migrateTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("Migrated ${memosMigratedOldStyle} old style memos.")
        log.info("Migrated ${memosMigratedNewStyle} new style memos.")
        log.info("duration: ${migrateTime} s")
    }

    def migrateOldStyleMemo(long id) {
        Deelnemer deelnemer = Deelnemer.findById(id)
        def memo = createMemoFromDeelnemer(deelnemer)
        if (memo) {
            memo.save()
            memosMigratedOldStyle += 1
        }
    }

    def migrateNewStyleMemo(int id) {
        Opmerking opmerking = Opmerking.findById(id)
        def memo = createMemo(opmerking)

        if (memo) {
            memo.save()
            memosMigratedNewStyle += 1
        }
    }

    Memo createMemo(Opmerking opmerking) {
        log.debug("opmerking: ${opmerking}")
        Memo memo = new Memo()

        memo.text = opmerking.tekst
        memo.timeStamp = opmerking.tijdstip.toLocalDateTime(LocalTime.MIDNIGHT)
        memo.processed = false;

        if (Deelnemer.existsWithId(opmerking.deelnemer_id)) {
            long targetPersonId = valorRepo.getPersonIdByValeaId(opmerking.deelnemer_id)
            Person targetPerson = new Person(id: targetPersonId)
            memo.target = targetPerson

            Deelnemer enteredByDeelnemer = Deelnemer.findByFullName(opmerking.gebruiker)
            long enteredByPersonId = valorRepo.getPersonIdByValeaId(enteredByDeelnemer.id)
            Person enteredBy = new Person(id: enteredByPersonId)
            memo.enteredBy = enteredBy
        }
        else {
            log.info("Skipped migrating '${memo.text}' for deelnemer id=${memo.id} because it was not migrated.")
            return null
        }

        return memo
    }


    Memo createMemoFromDeelnemer(Deelnemer deelnemer) {
        if (!deelnemer.opmerkingen || !deelnemer.opmerkingen.trim()) {
            return null
        }

        Memo memo = new Memo()
        memo.text = deelnemer.opmerkingen
        memo.processed = true;

        if (!valorRepo.deelnemerWithIdIsMigrated(deelnemer.id)) {
            log.info("Skipping migration of memo on ${deelnemer} because it was not migrated.")
            return null
        }
        long targetPersonId = valorRepo.getPersonIdByValeaId(deelnemer.id)
        memo.target = new Person(id: targetPersonId)

        if (memo.text.toLowerCase().contains("verenigingslid")) {
            memo.text = "[Check status] ${memo.text}"
            memo.enteredBy = getErikVisser()
            memo.timeStamp = new LocalDateTime("2012-07-07T12:00:00")
            return memo
        }

        if (memo.text.toLowerCase().contains("erelid")) {
            return null
        }

        memo.enteredBy = getGerrit()
        memo.timeStamp = new LocalDateTime("2011-07-07T12:00:00")
        memo.processed = true
        return memo
    }

    Person getErikVisser() {
        if (!erik) {
            long erikId = valorRepo.getPersonIdByPlin("634")
            erik = new Person(id: erikId)
        }
        return erik
    }

    Person getGerrit() {
        if (!gerrit) {
            long gerritId = valorRepo.getPersonIdByPlin("5070")
            gerrit = new Person(id: gerritId)
        }
        return gerrit
    }
}
