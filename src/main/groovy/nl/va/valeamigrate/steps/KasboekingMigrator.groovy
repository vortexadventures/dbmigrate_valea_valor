package nl.va.valeamigrate.steps

import nl.va.valeamigrate.repo.ValorRepo
import nl.va.valeamigrate.valea.Deelnemer
import nl.va.valeamigrate.valea.Kas_boeking
import org.joda.time.LocalDate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.va.model.PaymentStatus
import org.va.model.PaymentType
import org.va.valor.Payment
import org.va.valor.Person

/**
 * This class migrates the kasboekingen.
 */
@Component
class KasboekingMigrator {

    def static log = LoggerFactory.getLogger(KasboekingMigrator)

    def HANDBOEKING_M1_2013_DATUM = new LocalDate("2013-04-28")
    def HANDBOEKING_SUMDAG_2014_DATUM = new LocalDate("2014-03-15")


    @Autowired ValorRepo valorRepo

    int paymentsMigrated = 0
    int paymentsSkipped = 0

    Person yvo


    void run() {
        long startTime = System.currentTimeMillis()

        List<Integer> ids = Kas_boeking.listIdsfrom2013Onwards()
        log.info("Migrating ${ids.size()} kasboekingen.")
        ids.each({migrate(it)})

        long migrateTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("Migrated ${paymentsMigrated} boekingen.")
        log.info(" Skipped ${paymentsSkipped} boekingen because they were too old.")
        log.info("duration: ${migrateTime} s")
    }

    def migrate(long id) {

        Kas_boeking kasboeking = Kas_boeking.findById(id)
        def payment = createPayment(kasboeking)
        if (payment) {
            payment.save()
            paymentsMigrated += 1
        }
        else {
            paymentsSkipped += 1
        }

        if ((paymentsMigrated+ paymentsSkipped) % 500 == 0) {
            log.info ("at ${paymentsMigrated+ paymentsSkipped}")
        }
    }

    Payment createPayment(Kas_boeking kasboeking) {
        if (kasboeking.datum == null) {
            if (kasboeking.id < 5553) {
                log.debug("Skipping migration of kasboeking because it has no date and is old according to its id: ${kasboeking}")
                return null
            }
        }
        if (kasboeking.datum.isBefore(new LocalDate("2013-01-01"))) {
            log.debug("Skipping migration of kasboeking because it is too old: ${kasboeking}")
            return null
        }

        if (kasboeking.bedrag == 0) {
            log.debug("Skipping migration of kasboeking because amount is zero: ${kasboeking}")
            return null
        }

        Payment payment = new Payment()

        payment.valeaid = kasboeking.id
        payment.status = PaymentStatus.NEW
        payment.type = determineType(kasboeking)
        payment.amount = kasboeking.bedrag
        payment.amountOpen = kasboeking.bedrag
        payment.bookingDate = kasboeking.datum
        payment.bankBookingCode = null
        payment.description1 = parseOmschrijving(kasboeking.omschrijving1, payment)
        payment.description2 = nullTrim(kasboeking.omschrijving2)
        payment.description3 = nullTrim(kasboeking.omschrijving3)
        payment.description4 = null
        payment.description5 = null
        payment.description6 = null
        payment.ourSource = determineOurSource(payment)
        payment.theirAccountIban = null
        payment.theirAccountName = kasboeking.naam_tegenrekening?.replaceAll("  ", " ")
        payment.theirPerson = deriveTheirPerson(payment)
        payment.idealReference = null
        payment.handler = deriveHandler(payment)

        return payment
    }

    String parseOmschrijving(String input, Payment payment) {
        String processed = nullTrim(input)
        String omschrijving = (processed == null) ? "" : processed

        if (payment.bookingDate == HANDBOEKING_M1_2013_DATUM && (!omschrijving.startsWith("Betaling verenigingslidmaatschap"))) {
            return "Handmatige inboeking betalingen Moots 1 2013 met omschrijving: ${omschrijving}"
        }

        if (payment.bookingDate == HANDBOEKING_SUMDAG_2014_DATUM) {
            return "Handmatige inboeking betalingen Sumdag 2013 met omschrijving: ${omschrijving}"
        }

        return (omschrijving == "") ? "(geen omschrijving)" : omschrijving
    }

    Person deriveHandler(Payment payment) {
        if (payment.type == PaymentType.IDEAL) {
            return getYvo()
        }
        if ((payment.type == PaymentType.CASH || payment.type == PaymentType.PIN) && payment.description1.startsWith("Poortbetaling")) {
            String handlerFullName = payment.description2
            Deelnemer handlerDeelnemer = Deelnemer.findByFullName(handlerFullName);
            return valorRepo.getIdOnlyPersonForPlin(handlerDeelnemer.plin)
        }
        return null;
    }

    Person getYvo() {
        if (this.yvo == null) {
            this.yvo = valorRepo.getIdOnlyPersonForPlin("892")
        }
        return this.yvo
    }

    Person deriveTheirPerson(Payment payment) {
        if (payment.type == PaymentType.IDEAL) {
            return getPersonForPotentialPlin(payment.description1)
        }
        if ((payment.type == PaymentType.CASH || payment.type == PaymentType.PIN)&& payment.description1.startsWith("Poortbetaling")) {
                int startRelevance = payment.description1.indexOf("plin") + 5;
                String relevantDescription = payment.description1.substring(startRelevance)
//                if (relevantDescription.trim().isEmpty()) {
//                    log.info("poortbetaling zonder naam: valea id: ${payment.valeaid}")
//                    return null;
//                }
                return getPersonForPotentialPlin(relevantDescription)
        }
        if ((payment.bookingDate == HANDBOEKING_M1_2013_DATUM || payment.bookingDate == HANDBOEKING_SUMDAG_2014_DATUM) && payment.theirAccountName) {
            if (Deelnemer.existsWithFullName(payment.theirAccountName)) {
                if (payment.theirAccountName.trim().isEmpty()) {
                    log.info("handmatige boeking m1 2013 / sumdag 2014 zonder naam: valea id: ${payment.valeaid}")
                    return null;
                }
                Deelnemer deelnemer = Deelnemer.findByFullName(payment.theirAccountName);
                if (valorRepo.deelnemerWithIdIsMigrated(deelnemer.id)) {
                    return valorRepo.getIdOnlyPersonForPlin(deelnemer.plin)
                }
            }
        }
        return null
    }

    private Person getPersonForPotentialPlin(String relevantDescription) {
        String plin = parseForPlinNumber(relevantDescription)
        if (plin && valorRepo.deelnemerWithPlinIsMigrated(plin)) {
            return valorRepo.getIdOnlyPersonForPlin(plin)
        } else {
            return null
        }
    }

    String parseForPlinNumber(String description) {
        StringBuilder builder = new StringBuilder()
        for (char c : description.chars) {
            if (Character.isDigit(c)) {
                builder.append(c)
            }
            else {
                break;
            }
        }

        String firstDigits = builder.toString()
        if (firstDigits.isEmpty()) {
            return null;
        }
        int plinNumber = Integer.parseInt(firstDigits)
        if (plinNumber > 0) {
            return "${plinNumber}"
        }
        else {
            return null;
        }
    }

    String determineOurSource(Payment payment) {
        switch (payment.type) {
            case PaymentType.IDEAL : return "SISOW"
            case PaymentType.BANK : return "NL10RABO0112019064"
            case PaymentType.CASH : return "poortkassa nummer onbekend"
            case PaymentType.PIN : return "poortpin"
            default: throw new IllegalArgumentException("type ${type} unknown.")
        }
    }

    PaymentType determineType(Kas_boeking kasboeking) {
        if (kasboeking.kassa?.equalsIgnoreCase("ideal")) {
            return PaymentType.IDEAL;
        }
        if (kasboeking.kassa?.equalsIgnoreCase("rc")) {
            if (kasboeking.omschrijving1.startsWith("Poortbetaling PIN")) {
                return PaymentType.PIN
            }
            if (kasboeking.omschrijving1.startsWith("Poortbetaling cash")) {
                return PaymentType.CASH
            }

            if (kasboeking.omschrijving1.startsWith("Betaling verenigingslidmaatschap")) {
                return PaymentType.CASH
            }
            throw new RuntimeException("Failed to determine payment type for ${kasboeking}")
        }
        if (kasboeking.kassa?.equalsIgnoreCase("sumdag")) {
            return PaymentType.CASH
        }

        if (kasboeking.datum == HANDBOEKING_M1_2013_DATUM) {
            return PaymentType.CASH
        }

        if (kasboeking.naam_tegenrekening && kasboeking.naam_tegenrekening.trim()) {
            return PaymentType.BANK
        }
        else {
            return PaymentType.CASH
        }
    }

    String nullTrim(String input) {
        if (!input || input.trim().isEmpty()) {
            return null
        }
        return input.trim()
    }
}
