package nl.va.valeamigrate

import nl.va.valeamigrate.repo.ValorRepo
import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component

import javax.sql.DataSource

/**
 * This class defines a bean that exposes the Spring context to statically
 */
@Component
class StaticContext implements ApplicationContextAware {

    static ApplicationContext context;

    static JdbcTemplate valeaTemplate;


    static <T> T getBean(Class<T> clazz) {
        return context.getBean(clazz)
    }

    static def saveEntity(Object entity) {
        ValorRepo repo = getBean(ValorRepo)
        return repo.save(entity)
    }

    static def deleteEntity(Object entity) {
        ValorRepo repo = getBean(ValorRepo)
        return repo.delete(entity)
    }

    @Override
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;

        DataSource valeaDataSource = context.getBean("valeaDataSource");
        valeaTemplate = new JdbcTemplate(valeaDataSource);
    }
}
