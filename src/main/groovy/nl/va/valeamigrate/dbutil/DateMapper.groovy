package nl.va.valeamigrate.dbutil

import org.joda.time.LocalDate
import org.joda.time.LocalDateTime

import java.sql.ResultSet

/**
 * This class maps java.sql.Dates to and from Joda time
 */
class DateMapper {

    static LocalDate toLocalDate(ResultSet resultSet, String columnName) {
        Date date = resultSet.getDate(columnName)
        return (date == null) ? null: LocalDate.fromDateFields(date)
    }

    static LocalDateTime toLocalDateTime(ResultSet resultSet, String columnName) {
        Date date = resultSet.getTimestamp(columnName)
        return (date == null) ? null: LocalDateTime.fromDateFields(date)
    }
}
