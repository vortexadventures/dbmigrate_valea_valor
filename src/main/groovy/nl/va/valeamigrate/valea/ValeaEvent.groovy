package nl.va.valeamigrate.valea

import nl.va.valeamigrate.StaticContext
import nl.va.valeamigrate.dbutil.DateMapper
import org.joda.time.LocalDateTime
import org.springframework.jdbc.core.RowMapper
import org.va.model.Tariff

import java.sql.ResultSet
import java.sql.SQLException

/**
 * This class defines an event in Valea
 */
class ValeaEvent {

    public long id;
    public long deelnemer_id;

    public boolean crew;
    public boolean monster;
    public boolean ehbo;
    public boolean barlid;
    public boolean bestuur;
    public boolean k12;
    public boolean k16;
    public boolean speciaal_bedrag;

    public boolean inschrijvingsformulier;
    public boolean toegang_betaald;
    public boolean evenementlidmaatschap_betaald;

    public Integer tafels;
    public Integer banken;

    public int jaar;
    public int naam;

    public LocalDateTime incheck_date;
    public LocalDateTime uitcheck_date;

    public Tariff getTariff() {
        if (speciaal_bedrag) {
            return Tariff.ADJUSTED
        }
        if (crew) {
            return Tariff.CREW
        }
        if (this.monster) {
            return Tariff.CREW
        }
        if (ehbo) {
            return Tariff.FREE
        }
        if (barlid) {
            return Tariff.FREE
        }
        if (bestuur) {
            return Tariff.FREE
        }
        if (k12) {
            return Tariff.FREE
        }
        if (k16) {
            return Tariff.MINOR
        }

        return Tariff.FULL
    }


    static ValeaEvent findById(long id) {
        String sql = "SELECT * FROM events WHERE id = ?";
        try {
            ValeaEvent event = StaticContext.valeaTemplate.queryForObject(
                    sql,
                    new RowMapper<ValeaEvent>() {
                        @Override
                        public ValeaEvent mapRow(ResultSet rs, int row) throws SQLException {
                            ValeaEvent record = new ValeaEvent()
                            record.id = rs.getLong("id")
                            record.jaar = rs.getInt("jaar")
                            record.deelnemer_id = rs.getLong("deelnemer_id")
                            record.naam = rs.getInt("naam")


                            record.incheck_date = DateMapper.toLocalDateTime(rs, "incheck_date")
                            record.uitcheck_date = DateMapper.toLocalDateTime(rs, "uitcheck_date")

                            record.crew = rs.getBoolean("crew")
                            record.monster = rs.getBoolean("monster")
                            record.ehbo = rs.getBoolean("ehbo")
                            record.barlid = rs.getBoolean("barlid")
                            record.bestuur = rs.getBoolean("bestuur")
                            record.k12 = rs.getBoolean("k12")
                            record.k16 = rs.getBoolean("k16")
                            record.speciaal_bedrag = rs.getBoolean("speciaal_bedrag")
                            record.tafels = rs.getInt("tafels")
                            record.banken = rs.getInt("banken")
                            record.inschrijvingsformulier = rs.getBoolean("inschrijvingsformulier")
                            record.toegang_betaald = rs.getBoolean("toegang_betaald")
                            record.evenementlidmaatschap_betaald = rs.getBoolean("evenementlidmaatschap_betaald")

                            return record
                        }
                    },
                    id)

            return event
        }
        catch(Exception e) {
            throw new RuntimeException("Failed to retrieve event with id=${id}, due to ${e.getMessage()}.", e)
        }
    }

    static List<Long> findAllByYear(int year) {
        String sql = "SELECT id FROM events where jaar=?"
        return StaticContext.valeaTemplate.queryForList(sql, Long.class, year)
    }
}
