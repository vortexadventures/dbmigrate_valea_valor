package nl.va.valeamigrate.valea

import nl.va.valeamigrate.StaticContext
import nl.va.valeamigrate.dbutil.DateMapper
import org.joda.time.LocalDate
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.RowMapper

import java.sql.ResultSet
import java.sql.SQLException

/**
 * This class represents a Deelnemer record from Valea
 */
class Deelnemer {

    static Logger log = LoggerFactory.getLogger(Deelnemer)

    int id
    String voornaam
    String tussenvoegsels
    String achternaam
    String mv
    LocalDate geboortedatum
    String plin
    String straat_huisnr
    String postcode
    String woonplaats
    String telefoonnummers
    String land
    boolean voorkeur_post
    String email_adres
    String opmerkingen
    LocalDate lid_start
    LocalDate lid_eind
    boolean lidm_formulier_ontvangen

    static Deelnemer findByFullName(String fullName) {
        if (fullName == "Jeroen de Jong") {
            return findById(3155)
        }

        if (fullName == "Leo Batist") {
            return findById(2957)
        }

        if (fullName == "Peter Mulder") {
            return findById(3427)
        }

        String sql = "select id from deelnemers where concat(trim(concat_ws(' ', voornaam, tussenvoegsels)), ' ', achternaam) = ?"
        log.debug(fullName + ", " + sql);
        long id = StaticContext.valeaTemplate.queryForLong(sql, fullName)
        return findById(id)
    }

    String fullName() {
        if (tussenvoegsels == null || tussenvoegsels.trim().isEmpty()) {
            return "${voornaam} ${achternaam}"
        }
        else {
            return "${voornaam} ${tussenvoegsels} ${achternaam}"
        }
    }

    String toString() {
        return "[Deelnemer plin=${plin}, ${fullName()}, id=${id}, lid-start: ${lid_start}]"
    }


    static boolean existsWithId(long id) {
        return StaticContext.valeaTemplate.queryForInt("select count(*) from deelnemers where id=?", id) > 0;
    }

    static boolean existsWithFullName(String fullName) {
        String sql = "select count(*) from deelnemers where concat(trim(concat_ws(' ', voornaam, tussenvoegsels)), ' ', achternaam) = ?"
        return StaticContext.valeaTemplate.queryForInt(sql, fullName) > 0;
    }

    static Deelnemer findById(long id) {
        String sql = "SELECT * FROM deelnemers WHERE id = ?";
        try {
            Deelnemer deelnemer = StaticContext.valeaTemplate.queryForObject(
                    sql,
                    new RowMapper<Deelnemer>() {
                        @Override
                        public Deelnemer mapRow(ResultSet rs, int row) throws SQLException {
                            Deelnemer info = new Deelnemer()
                            info.id = rs.getLong("id")
                            info.plin = rs.getString("plin")
                            info.voornaam = rs.getString("voornaam")
                            info.tussenvoegsels = rs.getString("tussenvoegsels")
                            info.achternaam = rs.getString("achternaam")
                            info.mv = rs.getString("mv")

                            info.straat_huisnr = rs.getString("straat_huisnr")
                            info.postcode = rs.getString("postcode")
                            info.woonplaats = rs.getString("woonplaats")
                            info.telefoonnummers = rs.getString("telefoonnummers")
                            info.land = rs.getString("land")

                            info.voorkeur_post = rs.getBoolean("voorkeur_post")
                            info.lidm_formulier_ontvangen = rs.getBoolean("lidm_formulier_ontvangen")

                            info.geboortedatum = DateMapper.toLocalDate(rs, "geboortedatum")
                            info.email_adres = rs.getString("email_adres")
                            info.opmerkingen = rs.getString("opmerkingen")
                            info.lid_start = DateMapper.toLocalDate(rs, "lid_start")
                            info.lid_eind = DateMapper.toLocalDate(rs, "lid_eind")

                            return info
                        }
                    },
                    id)

            return deelnemer
        }
        catch(Exception e) {
            throw new RuntimeException("Failed to retrieve deelnemer with id=${id}, due to ${e.getMessage()}.", e)
        }
    }


    static List<Integer> listIds() {
        return StaticContext.valeaTemplate.queryForList("select id from deelnemers order by plin, id", Integer)
    }


}
