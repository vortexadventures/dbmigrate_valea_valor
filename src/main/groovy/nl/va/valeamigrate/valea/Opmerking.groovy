package nl.va.valeamigrate.valea

import nl.va.valeamigrate.StaticContext
import nl.va.valeamigrate.dbutil.DateMapper
import org.joda.time.LocalDate
import org.springframework.jdbc.core.RowMapper

import java.sql.ResultSet
import java.sql.SQLException

/**
 * This class represents a record in the "opmerkingen" table.
 */
class Opmerking {

    long id
    long deelnemer_id
    String tekst
    String gebruiker
    LocalDate tijdstip

    String toString() {
        return "[Opmerking id=${id}, deelnemer_id=${deelnemer_id}, gebruiker=${gebruiker}, tekst=${tekst}, tijdstip=${tijdstip}]";
    }

    static Opmerking findById(int id) {
        String sql = "SELECT * FROM opmerkingen WHERE id = ?";
        Opmerking opmerking = StaticContext.valeaTemplate.queryForObject(
                sql,
                new RowMapper<Opmerking>() {
                    @Override
                    public Opmerking mapRow(ResultSet rs, int row) throws SQLException {
                        Opmerking info = new Opmerking()
                        info.id = rs.getLong("id")
                        info.tekst = rs.getString("tekst")
                        info.gebruiker = rs.getString("gebruiker")
                        info.tijdstip = DateMapper.toLocalDate(rs, "tijdstip")
                        info.deelnemer_id = rs.getLong("deelnemer_id")

                        return info
                    }
                },
                id);
        return opmerking;
    }

    static List<Integer> listIds() {
        return StaticContext.valeaTemplate.queryForList("select id from opmerkingen order by id", Integer)
    }



}
