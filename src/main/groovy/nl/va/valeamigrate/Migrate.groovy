package nl.va.valeamigrate

import nl.va.valeamigrate.repo.FieldMapper
import nl.va.valeamigrate.repo.ValorRepo
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext
import org.va.valor.Membership
import org.va.valor.Person

/**
 * This class is the main class for running the migration from the Valea DB to the Valor DB.
 */
public class Migrate {

    static void main(def args) {
//        Person p = new Person()
//        p.id = 12
//        Membership m = new Membership()
//        m.person = p
//        m.formSigned = true
//        m.start = LocalDate.now()
//        m.time = LocalTime.now()
//        m.dateTime = LocalDateTime.now()
//
//        println FieldMapper.columList(Person)
//        println FieldMapper.columList(Membership)
//
//        println FieldMapper.valueList(m)
//
//        new ValorRepo().save(p);
//        new ValorRepo().save(m);


//        println FieldMapper.columList(Memo)
//        println FieldMapper.columList(Participation)
//        println FieldMapper.columList(Event)
//        println FieldMapper.columList(Product)
//        println FieldMapper.columList(Purchase)
//        println FieldMapper.columList(Transaction)
//        println FieldMapper.columList(TransactionMatch)
//        println FieldMapper.columList(User)


        ApplicationContext context = new ClassPathXmlApplicationContext("spring/application_context.xml");
        MigrationManager manager = context.getBean(MigrationManager)
        manager.run()
    }

}
